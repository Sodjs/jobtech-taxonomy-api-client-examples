#!/bin/bash
###############################################################################
#
# Map old taxonomy codes to new ones.
#
# Usage:
#
#   Options:
#     code-mapper.sh [-k api-key] [-s service-url] [-t type] [-c code] [-h]
#
#   Invocation to map a randomly selected code:
#     code-mapper.sh
#
#   Invocation to map a specific code:
#     code-mapper.sh -t occupation-group -c 9111
#
###############################################################################
DIR=$(exec 2>/dev/null;cd -- $(dirname "$0"); unset PWD; /usr/bin/pwd || /bin/pwd || pwd)
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi

## Load common functions
. "${DIR}/common.sh"



CODE=
TYPE=
MAPPER='https://raw.githubusercontent.com/JobtechSwe/elastic-importers/develop/importers/taxonomy/resources/taxonomy_to_concept.json'



usage() { echo "You can run this script without command line arguments for a"
          echo "randomly selected code, or supply a type and code with "
          echo "-t type -c code (for example -t country -c 99)."
          echo "Usage: $0 [-k api-key] [-s <service-url>] [-t type] [-c code] [-h]" 1>&2; exit 1; }



while getopts ":k:s:c:t:" o; do
    case "${o}" in
        k) APIKEY=${OPTARG}  ;;
        s) SERVICE=${OPTARG} ;;
        c) CODE=${OPTARG} ;;
        t) TYPE=${OPTARG} ;;
        *) usage ;;
    esac
done
shift $((OPTIND-1))



MAPFILE=$(common_mktemp)



echo -n "**** getting a copy of the mapper file..." >&2
curl --silent -o "${MAPFILE}" "${MAPPER}"
echo "!" >&2



## Randomly pick type unless given on the command line
if [ -z "${TYPE}" ]; then
    TYPES=($(jq 'keys' "${MAPFILE}" | tr -d '[,\[\]" ]'))
    RANDOM_INDEX=$(( ((RANDOM<<15)|RANDOM) % ${#TYPES[*]} ))
    TYPE=${TYPES[$RANDOM_INDEX]}
fi



## Randomly pick code unless given on the command line
if [ -z "${CODE}" ]; then
    CODES=($(jq ".\"${TYPE}\" | keys" "${MAPFILE}" | tr -d '[,\[\]" ]'))
    RANDOM_INDEX=$(( ((RANDOM<<15)|RANDOM) % ${#CODES[*]} ))
    CODE=${CODES[$RANDOM_INDEX]}
fi



echo "**** mapping old code type:${TYPE} code:${CODE}" >&2

jq ".\"${TYPE}\".\"${CODE}\"" < "${MAPFILE}"
