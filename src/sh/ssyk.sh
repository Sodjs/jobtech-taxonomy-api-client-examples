#!/bin/bash
###############################################################################
#
# Ectract a hierarchy of all SSYK concepts.
#
# Uses arrays - won't work with too old versions of Bash.
#
# Usage:
#
#   Options:
#     ssyk.sh [-k api-key] [-s service-url] [-h]
#
#   Invocation to map the ssyk levels:
#     ssyk.sh
#
###############################################################################
DIR=$(exec 2>/dev/null;cd -- $(dirname "$0"); unset PWD; /usr/bin/pwd || /bin/pwd || pwd)
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi

## Load common functions
. "${DIR}/common.sh"



common_getopts
MAPFILE=$(common_mktemp)



## Get list of ssyk types, sorted in ascending numeric order
SSYK_TYPES=($(common_call '/main/concept/types' | jq '.[] | select(startswith("ssyk"))' | sort -n))


for level in $(seq ${#SSYK_TYPES[*]} -1 2 )
do
    FROM_LEVEL=${level}
    TO_LEVEL=$(( $level - 1 ))

    echo "**** ssyk level ${FROM_LEVEL} to ${TO_LEVEL}" >&2
    common_call "/main/graph?edge-relation-type=broader&source-concept-type=ssyk-level-${FROM_LEVEL}&target-concept-type=ssyk-level-${TO_LEVEL}"  > "${MAPFILE}"
    common_map_printer "${MAPFILE}" ssyk-level-${FROM_LEVEL} ssyk-level-${TO_LEVEL}
done
