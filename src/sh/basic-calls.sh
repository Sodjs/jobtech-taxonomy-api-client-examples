#!/bin/bash
###############################################################################
#
# Basic test calls to all public endpoints.
#
# Usage:
#
#   Options:
#     basic-calls.sh [-k api-key] [-s service-url] [-h]
#
#   Invokation to make basic calls:
#     basic-calls.sh
#
###############################################################################
DIR=$(exec 2>/dev/null;cd -- $(dirname "$0"); unset PWD; /usr/bin/pwd || /bin/pwd || pwd)
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi

## Load common functions
. "${DIR}/common.sh"



common_getopts



common_call '/main/changes?after-version=2&to-version-inclusive=2&offset=0&limit=10'

common_call '/main/concept/types?version=2'

common_call '/main/concepts?offset=0&preferred-label=franska&id=47jp_tGV_oJR&limit=1&deprecated=false&type=sun-education-field-4&version=2'

common_call '/main/graph?edge-relation-type=narrower&source-concept-type=ssyk-level-1&target-concept-type=ssyk-level-2&offset=0&limit=10&version=2'

common_call '/main/relation/types'

common_call '/main/replaced-by-changes?after-version=2&to-version-inclusive=2'

common_call '/main/versions'

common_call '/specific/concepts/country?offset=0&limit=10&deprecated=false&type=country&version=2'

common_call '/specific/concepts/driving-licence?offset=0&limit=10&type=driving-licence&version=2'

common_call '/specific/concepts/isco?offset=0&limit=10&type=isco-level-4'

common_call '/specific/concepts/language?offset=0&limit=10&type=language'

common_call '/specific/concepts/municipality?offset=0&limit=10&type=municipality'

common_call '/specific/concepts/region?offset=0&limit=10&type=region'

common_call '/specific/concepts/sni-level?offset=0&limit=10&type=sni-level-1'

common_call '/specific/concepts/ssyk?offset=0&limit=10&type=ssyk-level-1'

common_call '/specific/concepts/sun-education-field?offset=0&limit=10&deprecated=false&type=sun-education-field-1'

common_call '/specific/concepts/sun-education-level?offset=0&limit=10&type=sun-education-level-1'

common_call '/suggesters/autocomplete?query-string=k%C3%A4rnkraft'
