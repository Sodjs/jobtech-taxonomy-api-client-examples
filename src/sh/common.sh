#!/bin/bash
###############################################################################
#
# This file contains helper functions and settings for the actual tests.
#
###############################################################################

## Activate strict mode
set -euo pipefail
IFS=$'\n\t'


## Common settings
SERVICE='https://taxonomy.api.jobtechdev.se/v1/taxonomy'
APIKEY=111



common_usage() { echo "You can run this script without command line arguments."
              echo "Usage: $0 [-k api-key] [-s <service-url>] [-h]" 1>&2; exit 1; }



function common_getopts {
    while getopts ":k:s:" o; do
        case "${o}" in
            k) APIKEY=${OPTARG}  ;;
            s) SERVICE=${OPTARG} ;;
            *) common_usage ;;
        esac
    done
    shift $((OPTIND-1))
}



function common_call {
    local endpoint="$1"

    echo -e "**** call ${endpoint}" >&2
    curl --silent -X GET --header 'Accept: application/json' \
         --header "api-key: ${APIKEY}" "${SERVICE}${endpoint}" | jq .
    echo >&2
}



function common_map_printer {
    local MAPFILE="$1"
    local SRC_LABEL="$2"
    local TRG_LABEL="$3"

    local MAP_SRC=($(jq '."taxonomy/graph"."taxonomy/edges" | sort_by(."taxonomy/target") | .[] | ."taxonomy/source"' < "${MAPFILE}" | tr -d '"'))
    local MAP_TRG=($(jq '."taxonomy/graph"."taxonomy/edges" | sort_by(."taxonomy/target") | .[] | ."taxonomy/target"' < "${MAPFILE}" | tr -d '"'))

    for i in $(seq 0 $(( ${#MAP_SRC[*]} - 1 )))
    do
        SRC_DESC=$(jq '."taxonomy/graph"."taxonomy/nodes"[] | select(."taxonomy/id" == "'${MAP_SRC[$i]}'") | ."taxonomy/preferred-label"' < "${MAPFILE}" | head -n 1 | tr -d '"')
        TRG_DESC=$(jq '."taxonomy/graph"."taxonomy/nodes"[] | select(."taxonomy/id" == "'${MAP_TRG[$i]}'") | ."taxonomy/preferred-label"' < "${MAPFILE}" | head -n 1 | tr -d '"')
        echo -e "$i\t${SRC_LABEL}\t${MAP_SRC[$i]}\t${SRC_DESC}"
        echo -e "$i\t${TRG_LABEL}\t${MAP_TRG[$i]}\t${TRG_DESC}\n"
    done
}



function common_join_by {
    local IFS="$1"
    shift
    echo "$*"
}



function common_mktemp {
    local FILE=$(mktemp)
    trap "rm -f ${FILE}" exit
    echo "${FILE}"
}
