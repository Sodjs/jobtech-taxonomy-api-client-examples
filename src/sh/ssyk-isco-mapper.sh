#!/bin/bash
###############################################################################
#
# Map ssyk codes to isco codes.
#
# Usage:
#
#   Options:
#     ssyk-isco-mapper.sh [-k api-key] [-s service-url] [-h]
#
#   Invocation to map ssyk to isco codes:
#     ssyk-isco-mapper.sh
#
###############################################################################
DIR=$(exec 2>/dev/null;cd -- $(dirname "$0"); unset PWD; /usr/bin/pwd || /bin/pwd || pwd)
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi

## Load common functions
. "${DIR}/common.sh"



common_getopts
MAPFILE=$(common_mktemp)



common_call '/main/graph?edge-relation-type=related&source-concept-type=ssyk-level-4&target-concept-type=isco-level-4' > "${MAPFILE}"

common_map_printer "${MAPFILE}" ssyk-level-4 isco-level-4
